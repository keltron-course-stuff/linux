#!/bin/bash
#Program:Ascending order of numbers entered from the commandline
arg_num=$#
num_array=( $* )
if [ $arg_num -lt 1 ]
then
 echo "Invalid number of arguments"
else
 sum=0
 for((i=0;$i<$arg_num;i=$i+1))
 do
  for((j=0;$j<$arg_num;j=$j+1))
  do
   if [ ${num_array[$i]} -lt ${num_array[$j]} ]
   then
    tmp=${num_array[$j]}
    num_array[$j]=${num_array[$i]}
    num_array[$i]=$tmp
   fi
  done
 done
 echo "The list $* after ascending order sorting:"
 echo "${num_array[*]}"
fi
