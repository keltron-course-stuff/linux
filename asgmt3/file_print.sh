#!/bin/bash
#Program:To print contains of file from given line number to next given number of lines
echo "Enter a filename:"
read filename
if [ -e $filename -a -f $filename ]
then
 echo "Enter the starting line number:"
 read strt
 echo "Enter the ending line number:"
 read end
 let end=$end-1
 let limit=$end-$strt
 out=`head -$end $filename|tail -$limit`
 echo $out 
else
	echo "There exists no file named $filename."
fi
