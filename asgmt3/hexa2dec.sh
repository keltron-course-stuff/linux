#!/bin/bash
#Program: Converting hexadecimal to decimal
echo "Enter a hexadecimal value:"
read hexa
length=${#hexa}
sum=0
let len=$length-1
for((i=0;i<$length;i=$i+1))
do
 digi=${hexa:$i:1}
 case $digi in
 "0")
        let sum=$sum;;
 "1")
	let tem=16**$len
	let val=1*$tem
	let sum=$sum+$val;;
 "2")
        let tem=16**$len
        let val=2*$tem
        let sum=$sum+$val;;
 "3")
	let tem=16**$len
        let val=3*$tem
        let sum=$sum+$val;;
 "4")
	let tem=16**$len
        let val=4*$tem
        let sum=$sum+$val;;
 "5")
	let tem=16**$len
        let val=5*$tem
        let sum=$sum+$val;;
 "6")
	let tem=16**$len
        let val=6*$tem
        let sum=$sum+$val;;
 "7")
	let tem=16**$len
        let val=7*$tem
        let sum=$sum+$val;;
 "8")
	let tem=16**$len
        let val=8*$tem
        let sum=$sum+$val;;
 "9")
	let tem=16**$len
        let val=9*$tem
        let sum=$sum+$val;;
 "A" | "a")
	let tem=16**$len
        let val=10*$tem
        let sum=$sum+$val;;
 "B" | "b")
	let tem=16**$len
        let val=11*$tem
        let sum=$sum+$val;;
 "C" | "c")
	let tem=16**$len
        let val=12*$tem
        let sum=$sum+$val;;
 "D" | "d")
	let tem=16**$len
        let val=13*$tem
        let sum=$sum+$val;;
 "E" | "e")
	let tem=16**$len
        let val=14*$tem
        let sum=$sum+$val;;
 "F" | "f")	
	let tem=16**$len
        let val=15*$tem
        let sum=$sum+$val;;
 esac
 let len=$len-1
done
echo "The decimal equivalent of ${hexa[*]} is $sum."
