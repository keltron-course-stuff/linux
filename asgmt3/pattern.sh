#!/bin/bash
<<PROGRAM-
Shell script for print the given below pattern, here 5 is entered as a command line argument
1
1 2
1 2 3
1 2 3 4
1 2 3 4 5
1 2 3 4
1 2 3
1 2
1
PROGRAM-

arg_num=$#
limit=$1
if [ $arg_num -lt 1 ]
then
	echo "Invalid number of arguments"
else
	for((i=1;$i<=$limit;i=$i+1))
	do
        	for((j=1;$j<=$i;j=$j+1))
		do
		 s[$j]=$j;	
		done	
		echo ${s[*]}
	done
	for((i=$limit-1;$i>=1;i=$i-1))
        do
		s=()
                for((j=1;$j<=$i;j=$j+1))
                do
                 s[$j]=$j;
                done
                echo ${s[*]}
        done
fi
