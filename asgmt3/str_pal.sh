#!/bin/bash
#Program:To check whether an entered string is palindrome or not
echo "Enter a string:"
read string
s=${#string}
flag=0
for((i=$s-1,j=0;i>=0;i=$i-1,j=$j+1))
do
 a[$j]=${string:$i:1};
done
for((i=0;$i<$s;i=$i+1))
do
	if [ ${string:$i:1} != ${a[$i]} ]
	then
        	flag=1 
	fi
done
if [ $flag -eq 0 ]
then
	echo "$string is a palindrome."
else
	echo "$string is not a palindrome."
fi
