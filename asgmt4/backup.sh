#!/bin/bash
<<Program-
 Shell script called backup.sh that takes a single argument, which should be the name of an existing directory. 
 If the directory does not exists, an appropriate message should be displayed. Then the script should create a bzipped archive file
 of the folder using tar program. The file name of the archive should be in the format foldername_17.09.2010_09.00.tar.bz2
 where 17.09.2010 and 09.00 represents the date and time at which the archive is created. The archive should be stored in 
 a folder called backups in the home folder of the user. If backups does not exist, the script should create it.
Program-

if [ $# -eq 1 ]
then
	fol_path=$1
	if [ -e $fol_path ]
	then
		dir_name=`basename $fol_path`
		dest_name="$dir_name""_"`date +%d.%b.%Y`"_"`date +%H.%M".tar.bz2"`
		tar cvzf $dest_name -C / $fol_path
		if [ ! -e ~/backup ]
		then
		 mkdir ~/backup		
		fi
		mv $dest_name ~/backup/
	else
		echo "The folder $fol_path does not exist."
	fi
else
	echo "Invalid number of arguments."
fi
