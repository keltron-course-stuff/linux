#!/bin/bash
#Program:program to count the number of files, directories and links in any folder in the linux file system hierarchy specified as command line argument. 
#    If the folder does not exist, then an appropriate message should be displayed
if [ $# -eq 1 ]
then
	fil_count=0
	dir_count=0
	lnk_count=0
	fol_path=$1
	if [ -e $fol_path ]
	then
		cd $fol_path
		for x in *
		do
			if [ -f $x -a -h $x ]
			then
				let lnk_count=$lnk_count+1
			elif [ -d $x ]
                	then
                        	let dir_count=$dir_count+1
			elif [ -f $x ]
                	then
                        	let fil_count=$fil_count+1
			fi
		done
		echo "In the directory-$fol_path: number of files=$fil_count, directories=$dir_count, and links=$lnk_count."
	else
		echo "The folder $fol_path does not exist."
	fi
else
	echo "Invalid number of arguments."
fi
