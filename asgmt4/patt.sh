#!/bin/bash
<<PROGRAM-
 To print the following pattern:
 |_
 | |_
 | | |_
 | | | |_
 | | | | |_
PROGRAM-

echo "Enter the number of lines of the pattern you want to display"
read n
for((j=1;j<=$n;j++))
do 
 let end=2*$j
 let end=$end-1
 for((i=1;i<=end;i=$i+1))
 do
 if [ $i -eq $end ]
 then
 	echo "|_"
 else
 	let rem=$i%2
 	if [ $rem -eq 0 ]
	then
		echo -n " "
	else
		echo -n "|"
	fi
 fi
 done
done
