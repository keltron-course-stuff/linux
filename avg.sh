#!/bin/bash
#Program:Average of numbers entered from the commandline
arg_num=$#
num_array=( $* )
if [ $arg_num -lt 1 ]
then
 echo "Invalid number of arguments"
else
 sum=0
 for((i=0;$i<$arg_num;i=$i+1))
 do
  array_elt=${num_array[$i]}
  let sum=$sum+$array_elt 
  #let sum=$sum+${num_array[$i]}
 done
 let avg=$sum/$arg_num
 echo "The average of $* is $avg."
fi
