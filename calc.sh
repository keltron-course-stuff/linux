#!/bin/bash
if [ $# -ne 3 ]
then
 echo "Invalid number of arguments."
else 
  if [ $2 = 'x' ] 
  then
   result=`echo "$1*$3" |bc` 
  else
   result=`echo "$1$2$3" |bc`
  fi 
fi
#echo "$1 $2 $3=$result"
if [ $result ]
then
 echo "$1 $2 $3=$result"
fi
