#!/bin/bash
echo "Enter a positive integer:"
read num
if [ $num -eq 0 -o $num -eq 1 ]
then
 fact=1
else
 for((fact=1,cnt=2;$cnt<=$num;cnt=$cnt+1))
 do
  let fact=$fact*$cnt
 done
fi
echo "$num!=$fact"
