#!/bin/bash
#Program:To check whether given file exists or not
echo "Enter a filename:"
read filename
if [ -e $filename -a -f $filename ]
then
 echo "A file named $filename exists."
else 
 echo "A file named $filename does not exist."
fi
