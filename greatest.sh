#!/bin/bash
#Program:Greatest of 3 numbers entered from the commandline
arg_num=$#
num_array=( $* )
if [ $arg_num -lt 3 ]
then
 echo "Invalid number of arguments."
else
 if [ $1 -ge $2 ]
 then 
  big=$1
 elif [ $2 -ge $3 ]
 then
  big=$2
 elif [ $3 -ge $1 ]
 then
  big=$3 
 fi
 echo "$big is the greatest."
fi
