#!/bin/bash
#Program:
<<PGMLOGIC
If the year is divisible by 4, it is a leap year. But, if it is divisible by 
100, it is not. If the year is divisible by 400, it is a leap year.
PGMLOGIC

echo "Enter an year:"
read yr;
function leap()
{
	year=$1;
        let tmp1=$year%400
        let tmp2=$year%100
        let tmp3=$year%4
	if [ $tmp1 -eq 0 ]
	then
  		echo "$year is a Leap Year."
	elif [ $tmp2 -eq 0 ]
	then
		echo "$year is not a Leap Year."
	elif [ $tmp3 -eq 0 ]
	then
		echo "$year is a Leap Year."
	else
		echo "$year is not a Leap Year."
	fi
}
leap $yr
