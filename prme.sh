#!/bin/bash
#program:To find the prime numbers between 1 and 100
echo "The prime numbers between 1 and 100 are:"
for((num=2;$num<=100;num=$num+1))
do
 flag=0
 i=2
 let limit=$num/2
 while [ $i -le $limit ]
 do
  let nxt=$num%$i
  if [ $nxt -eq 0 ]         
  then
      flag=1;
      break;
  fi
  let i=$i+1                        
 done
 if [ $flag -ne 1 ]
 then
  echo "$num,"
 else
  continue
 fi
done

