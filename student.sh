#!/bin/bash
echo "Enter student's name:"
read name
echo "Enter maximum marks:"
read max_marks
echo "Enter subject name:"
read sub_name
echo "Enter marks obtained:"
read mark_obt
#Grading System
if [ $mark_obt -ge 90 ]
then
  echo "Grade of $name for the subject $sub_name:S "
elif [ $mark_obt -ge 80 -a  $mark_obt -le 89 ]
then
 echo "Grade of $name for the subject $sub_name:A"
elif [ $mark_obt -ge 70 -a  $mark_obt -le 79 ]
then
 echo "Grade of $name for the subject $sub_name:B"
elif [ $mark_obt -ge 50 -a  $mark_obt -le 69 ]
then
 echo "Grade of $name for the subject $sub_name:C"
elif [ $mark_obt -lt 50 ]
then
 echo "Grade of $name for the subject $sub_name:F"
fi
